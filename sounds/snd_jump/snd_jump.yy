{
  "audioGroupId": {
    "name": "audiogroupsound",
    "path": "audiogroups/audiogroupsound",
  },
  "soundFile": "snd_jump.wav",
  "name": "snd_jump",
  "conversionMode": 0,
  "compression": 0,
  "type": 1,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "duration": 0.48,
  "volume": 1.0,
  "preload": false,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}