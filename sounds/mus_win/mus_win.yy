{
  "audioGroupId": {
    "name": "audiogroupmusic",
    "path": "audiogroups/audiogroupmusic",
  },
  "soundFile": "mus_win.ogg",
  "name": "mus_win",
  "conversionMode": 0,
  "compression": 0,
  "type": 1,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "duration": 7.204172,
  "volume": 1.0,
  "preload": false,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}