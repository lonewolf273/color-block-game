{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "mus_level3.ogg",
  "name": "mus_level3",
  "conversionMode": 0,
  "compression": 3,
  "type": 1,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "duration": 42.6805458,
  "volume": 1.0,
  "preload": false,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}