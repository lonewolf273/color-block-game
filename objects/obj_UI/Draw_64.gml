/// @description Insert description here
// You can write your code in this editor
draw_set_halign(fa_left)
draw_set_valign(fa_top)

draw_rectangle_color(padding, 0, bkg_width + padding, bkg_height, bkg_color, bkg_color, bkg_color, bkg_color, false)

line = 0

function DrawStat(_statName, _stat)
{
	var str = _statName + ": " + string(_stat)
	draw_text_color(padding, height * line, str, draw_color, draw_color, draw_color, draw_color, 1)
	line = line + 1
}

if (show_fps) DrawStat("FPS", fps)
if (show_level_number) DrawStat("Level", global.levelList.current + 1)
if (show_stopwatch)
{
	DrawStat("Time", stopwatch)
	DrawStat("Room Time", room_stopwatch)
}