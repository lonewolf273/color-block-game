/// @description Insert description here
// You can write your code in this editor

if (not global.paused)
{
	play = global.levelList.current > 0 and not global.levelList.completed

	if (play)
	{
		stopwatch += CONSTANT_TIME
		room_stopwatch += CONSTANT_TIME
	}
}