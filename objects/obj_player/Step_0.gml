/// @description Player Movement
// You can write your code in this editor

if (keyboard_check_pressed(vk_f4))
{
	global.Display.ToggleFullscreen()
}

//show_debug_message(window_get_height())

if (INPUT_RESTART_PRESSED) 
{
	global.levelList.RestartLevel()
}


if (not global.paused)
{
	if (place_meeting(x, y, obj_hazard))
	{
		Die()	
	}
	
	move.MovementUpdate()
	move.TickTimers()
	
	if (INPUT_PAUSE_PRESSED) 
		instance_create_depth(x, y, depth, obj_paused)
}

function Die()
{
	global.levelList.RestartLevel()	
}
