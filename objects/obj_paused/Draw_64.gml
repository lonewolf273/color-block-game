/// @description Insert description here
// You can write your code in this editor

//make black background
var fnt = draw_get_font()
draw_set_font(fnt_menu)

draw_set_alpha(background_alpha)
draw_rectangle_color(0, 0, display_get_gui_width(), display_get_gui_height(), c_black, c_black, c_black, c_black, false)
draw_set_alpha(1)

// make the font
height = display_get_gui_height()
width = display_get_gui_width()


draw_set_halign(fa_center)
draw_set_valign(fa_middle)


h = FONT_HEIGHT
_line = 0
h_base = height / 2


function DrawMenuOption(_option, _selected = false)
{
	if (_selected)
	{
		draw_set_color(selected_color)	
	}
	else
	{
		draw_set_color(font_color)	
	}
	
	draw_text(width / 2, h * _line + h_base, _option)
	_line += 1
}

for (var i = 0; i < menu.Length(); ++i)
{
	var _s = i == menu.index
	DrawMenuOption(menu.options[i], _s)
}

draw_set_font(fnt)