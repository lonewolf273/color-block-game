// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PauseMenu(_inst) constructor
{
	options =
	[
		"RESUME",
		"RESTART LEVEL",
		"RESTART RUN",
		"QUIT"
	]
	
	index = 0
	inst = _inst
	
	static Length = function()
	{
		return array_length(options)
	}
	
	static Next = function()
	{
		index += 1
		if (array_length(options) <= index)
			index = 0
	}
	
	static Previous = function()
	{
		index -= 1
		if (index < 0)
			index = array_length(options) - 1
	}
	
	static Select = function()
	{
		var _s = options[index]
		if (_s == "RESUME")	
			Resume()
		else if (_s == "RESTART LEVEL")
			RestartLevel()
		else if (_s == "RESTART RUN")
			RestartRun()
		else
			Quit()
	}
	
	static Resume = function()
	{
		instance_destroy(inst)
	}
	
	static RestartLevel = function()
	{
		global.paused = false
		global.levelList.RestartLevel()
	}
	
	static RestartRun = function()
	{
		global.paused = false
		global.levelList.RestartList()
	}
	
	static Quit = function()
	{
		global.paused = false
		game_end()	
	}
}