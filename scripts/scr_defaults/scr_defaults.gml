/*
	NAME:			SCR_DEFAULTS.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		1.0.0
	LAST MODIFIED:	3 SEPTEMBER 2020
*/

/// Please feel free to use this as default settings for controls and globals.

function NextScheme()
{
	global.Colors = global.Schemes.NextScheme()	
}

function PreviousScheme()
{
	global.Colors = global.Schemes.PreviousScheme()	
}

//GLOBAL.CONTROLS: The controller struct to be used for the game.
global.Configs = {
	normal: new Config().Default(), //the default configuration
}

//GLOBAL.CONTROLS: The controller struct to be used for the game.
global.Controls = new Controller(global.Configs.normal, 0)
global.Schemes = new ColorSchemeList()
global.Colors = global.Schemes.GetScheme()
global.paused = false


global.Display = new DisplaySetter()

function DisplaySetter() constructor
{
	res_h = 700
	res_w = 1200
	fullscreen = window_get_fullscreen()
	
	window_height = 700
	window_width = 1200
	
	static ToggleFullscreen = function()
	{
		if (fullscreen)
		{
			display_set_gui_size(window_width, window_height)			
		}
		else
		{
			window_height = window_get_height()
			window_width = window_get_width()
			display_set_gui_size(display_get_width(), display_get_height())
		}
		
		fullscreen = not fullscreen
		window_set_fullscreen(fullscreen)
		if (not fullscreen)
		{
			window_set_size(window_width, window_height)
			window_set_position(display_get_width() / 2 - window_width / 2, display_get_height() / 2 - window_height / 2)
		}
	}
}