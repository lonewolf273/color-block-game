/*
	NAME:			SCR_JOYPAD_CONTROL.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		2.0.0
	LAST MODIFIED:	3 SEPTEMBER 2020
*/

///JOYPADBUTTON(name, button, slot)
	// a joypad button to check and check pressed against
function JoyPadButton(_name, _button, _slot) constructor {
	name = _name
	button = _button
	slot = _slot
	
	static IsConnected = function() {
		return slot + 1 <= gamepad_get_device_count() and gamepad_is_connected(slot)	
	}
	
	static CheckPressed = function() {
		return IsConnected() and gamepad_button_check_pressed(slot, button);	
	}
	
	static Check = function() {
		return IsConnected() and gamepad_button_check(slot, button)	
	}
}


enum Axis {
	LX,
	LY,
	RX,
	RY	
}

function JoystickAxis(_name, _stick, _slot) constructor {
	name = _name
	axis = _stick
	slot = _slot
	deadzone = JOYSTICK_DEADZONE
	
	
	static IsConnected = function() {
		return slot + 1 <= gamepad_get_device_count() and gamepad_is_connected(slot)	
	}
	
	static SetDeadzone = function(_d) {
		if (_d < 0) _d = -_d
		if (_d > 0.5) _d = 0.5
		deadzone = _d
	}
	
	static GetAxisValue = function(_flip) {
		if (not IsConnected()) return undefined;
		
		var _axis = gamepad_axis_value(slot, axis)
		
		if (abs(_axis) < deadzone) return 0
		
		if(_flip) return -_axis
		
		return _axis
	}
	
	static GetValueBinary = function(_flip) {
		var _g = GetAxisValue(_flip)
		
		if (_g == undefined) return undefined
		
		return _g == 0 ? 0 : sign(_g)
	}
}