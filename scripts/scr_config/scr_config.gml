/*
	NAME:			SCR_CONFIG.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		2.0.0
	LAST MODIFIED:	4 SEPTEMBER 2020
*/


#region Action List

//ACTION: The list of actions in the game
	// useful for telling the game what type of actions the player will be doing for response
	// adjust for your own custom action list
enum Action {
	LEFT,
	RIGHT,
	UP, 
	DOWN,
	JUMP,
	DASH,
	NEXT,
	PREVIOUS,
	
	_length //GETS THE LENGTH OF THE ACTION LIST. PLEASE DO NOT DELETE THIS ACTION!
}

//ActionString(a) turn your actions into strings
function ActionString(a) {
	switch(a) {
		case Action.LEFT:
			return "left"
		case Action.RIGHT:
			return "right"
		case Action.UP:
			return "up"
		case Action.DOWN:
			return "down"
		case Action.JUMP:
			return "jump"
		case Action.DASH:
			return "dash"
		case Action.NEXT:
			return "next"
		case Action.PREVIOUS:
			return "previous"
		default:
			return undefined
	}
}
#endregion

#region Config Struct for configuration

///CONFIGACITON(KEY, MOUSE, PAD)
	//used to assign keys and pads to actions
function ConfigAction(_key, _mouse, _pad) constructor {
	key	= _key;
	isMouse = _mouse;
	pad	= _pad

}

///CONFIG()
	//the base struct for different configuations. 
	//feel free to use the bottom of the struct to create different types of configs.
function Config () constructor {
	
	// SetActionKey(_actionType, _key): sets the aciton to the keyboard button
		// hopefully also checks if it's a mouse click or not
	static SetActionKey = function (_actionType, _key) {
		
		var _isMouse = (_key == mb_left || _key == mb_right) //check if it's a mouse click
		
		var _name = ActionString(_actionType) 
		
		if (_name == undefined) 
			return;
		
		var p = -1
		
		if (variable_struct_exists(self, _name))
			p = variable_struct_get(self, _name).pad 
		
		variable_struct_set(self, _name, new ConfigAction(_key, _isMouse, p)) 
	}
	
	// SetActionPad(_actionType, _key): sets the aciton to the joypad button
	static SetActionPad = function (_actionType, _pad) {
		var _name = ActionString(_actionType)
		
		if (_name == undefined) 
			return;
	
		var _v = new ConfigAction(1, 0, 0)
		
		if (variable_struct_exists(self, _name))
			_v = variable_struct_get(self, _name);
		
		var _k = _v.key
		var _m = _v.isMouse

		variable_struct_set(self, _name, new ConfigAction(_k, _m, _pad))
		
	}
	
	// SetKeys(...)
		// Sets the keyboard controls 
		//please adjust this based on the number of actions in your game. 
	static SetKeys = function (_left, _right, _up, _down, _action, _cancel, _previous, _next) {

		SetActionKey(Action.LEFT, _left) 
		SetActionKey(Action.RIGHT, _right)
		SetActionKey(Action.UP, _up)
		SetActionKey(Action.DOWN, _down)
		SetActionKey(Action.JUMP, _action)
		SetActionKey(Action.DASH, _cancel)
		SetActionKey(Action.PREVIOUS, _previous)
		SetActionKey(Action.NEXT, _next)
	}
	
	// SETPAD(...)
		// Sets the joypad controls 
		//please adjust this based on the number of actions in your game. 
	static SetPad = function (_left, _right, _up, _down, _action, _cancel, _previous, _next) {
		SetActionPad(Action.LEFT, _left) 
		SetActionPad(Action.RIGHT, _right)
		SetActionPad(Action.UP, _up)
		SetActionPad(Action.DOWN, _down)
		SetActionPad(Action.JUMP, _action)
		SetActionPad(Action.DASH, _cancel)
		SetActionPad(Action.PREVIOUS, _previous)
		SetActionPad(Action.NEXT, _next)
	}
	
	//CREATE()
		// creates a blank configuration
	static Create = function() {
		var o = new Config()
		
		var _c = new ConfigAction(vk_anykey, false, gp_start)

		for(var i = 0; i < Action._length; ++i)
		{		
			variable_struct_set(o, ActionString(i), _c) //Set Acitons	
		}	
		
		
		return o
	}
	
	//default configuration
	static Default = function() {
		var o = Create() //create a parepared config
		
		//set defaults.
			//			Left		Right		Up			Down		Jump			Dash			Previous			Next
		o.SetKeys	(	vk_left,	vk_right,	vk_up,		vk_down,	"Z",			"X",			"1",				"2"					)
		o.SetPad	(	gp_padl,	gp_padr,	gp_padu,	gp_padd,	gp_face1,		gp_face3,		gp_shoulderl,		gp_shoulderr		)
		
		return o
		
	}
	
}
#endregion

