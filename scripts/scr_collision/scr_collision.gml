/// @desc 
/// @param obj Object to pull out of the collider.
/// @param passthrough whether or not the object can be passed through from below. False by default
function scr_collision() {

	var ins = instance_place(x, y, argument[0])
	var pass = false


	
	if (argument_count > 1) pass = argument[1]

	if (not ins) return undefined;

	var too_far_left = x - ins.x > 0 //check if the player went too far to the left
	var too_far_up = y - ins.y > 0 //check if the player went too far up

	var xpush = 0, ypush = 0;
	do
	{
		var diff_x = too_far_left ? ins.bbox_right - bbox_left : bbox_right - ins.bbox_left //calculates how far the player went; moves it.
		var diff_y = too_far_up ? ins.bbox_bottom - bbox_top : bbox_bottom - ins.bbox_top //calculates how far the player went; moves it.
	
		if(xpush == 0) xpush = sign(diff_x)
		if(ypush == 0) ypush = sign(diff_y)
	
		if(diff_y < diff_x) y -= diff_y + (COLLISION_THRESHOLD == 0 ? xpush : COLLISION_THRESHOLD)
		else x -= diff_x + (COLLISION_THRESHOLD == 0 ? xpush : COLLISION_THRESHOLD)
	
	} until (not instance_place(x, y, ins)) 
}
