

function GameTimer(_maxTimer) constructor
{
	maxTime = _maxTimer
	
	currentTime = 0
	eventTrigger = false
	
	static ResetTimer = function()
	{
		currentTime = maxTime	
	}
	
	static GetTime = function()
	{
		return currentTime;
	}
	
	static RefreshTime = function()
	{
		if(not TimeOut())
		{
			currentTime = max(0, currentTime - CONSTANT_TIME)
			eventTrigger = currentTime == 0 //have the event note that it triggered
		}
		else eventTrigger = false
		return TriggerCheck()
	}
	
	static TimeOut = function()
	{
		//show_debug_message(GetTime())
		return GetTime() <= 0	
	}
	
	static TriggerCheck = function()
	{
		return eventTrigger
	}
		
}