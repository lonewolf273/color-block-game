

var l = 
[
	rm_lvl_1,
	rm_lvl_2,
	rm_lvl_3,
	rm_lvl_4,
	rm_lvl_5,
	rm_lvl_6,
	rm_lvl_7,
	rm_lvl_8,
	//rm_lvl_9,
	rm_lvl_10,
	rm_lvl_11,
	rm_lvl_12
]

global.levelList = new LevelList(l)

global.levelList.current = 0


function LevelList(_list) constructor
{
	list = _list
	current = 0
	completed = false
	
	static CurrentLevel = function()
	{
		return list[current]	
	}
	
	static GetNextLevel = function()
	{
		if (array_length(list) <= current + 1)
			return noone
		
		return list[current + 1]
	}
	
	static StartNextLevel = function()
	{
		if (GetNextLevel() == noone) 
		{
			completed = true
			return false
		}
		
		current += 1
		StartLevel()
		return true
		
	}
	
	static StartLevel = function()
	{
		room_goto(CurrentLevel())
	}

	static RestartLevel = function()
	{
		completed = false
		room_restart()
	}
	
	static RestartList = function()
	{
		completed = false
		current = 0
		StartLevel()
	}
}