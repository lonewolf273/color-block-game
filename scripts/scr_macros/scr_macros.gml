#region Time Macros
#macro CONSTANT_TIME delta_time / power(10,6)
#macro FRAME_TIME 1/room_speed * CONSTANT_TIME
#endregion Time Macros

#region Threshold Macros
#macro GROUNDED_THRESHOLD 1 //the amount of pixels down to check if a player is grounded.
#macro COYOTE_TIME 3 * FRAME_TIME//the amount of frames from the moment the player isn't grounded
#macro COLLISION_THRESHOLD 1
#endregion Threshold Macros

#region Physics Macros
#macro FRICTION_AIR 0.25
#macro FRICTION_GROUND 0.35
#macro G 1.4
#macro TERMINAL_VELOCITY 60
#endregion Physics Macros

#region Object Macros
#macro OPASSABLE obj_passable
#macro OSOLID obj_solid

#endregion

#region Font Macros

#macro FONT_HEIGHT string_height("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

#endregion