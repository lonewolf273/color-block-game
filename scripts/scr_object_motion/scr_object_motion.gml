/*
	NAME:			SCR_OBJECT_MOTION.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		0.1.0
	LAST MODIFIED:	18 NOVEMBER 2020
*/

enum MoveState
{
	MOVE,
	DASH	
}

function PlayerMotion(_inst, _accel, _walk, _jumpSpeed, _passthrough = false, _dashMult = 2, _dashtime = 0.1) constructor 
{
	instanceID = _inst //instance object
	
	velocity = new Vector()
	
	control = 1

	acceleration = _accel
	walkSpeed = _walk
	dashMultiplier = _dashMult
	gravAffect = true
	jumpSpeed = _jumpSpeed
	dashAir = true
	
	moveState = MoveState.MOVE
	
	dash_timer = new GameTimer(_dashtime)
	
	passing = _passthrough
	

#region Instance Functions

	static Instance = function()
	{
		return instanceID.object_index	
	}
	
	static GetPosition = function()
	{
		return new Vector(Instance().x, Instance().y)	
	}

#endregion

#region Refresh Functions
	//TickTimers
		// Refreshes the ticker for each timer.
		// TODO: make a timer structure
	static TickTimers = function()
	{	
		dash_timer.RefreshTime()
	}
#endregion
	
	// CheckGround()
		// checks if the player is right on the ground
		// use the grounded threshhold to see if moving the player by that amount in the Y direction will collide them with a valid object 
	static CheckGround = function(_passCount = true)
	{
		var _move = self
		
		with (Instance())
		{
			
			if (place_meeting(x, y + GROUNDED_THRESHOLD, OSOLID))
				return true
			
			if (_passCount)
			{
				
				if (not _move.passing)
				{
					
					var p = place_meeting(x, y + GROUNDED_THRESHOLD, OPASSABLE)
					/*if (p)
					{
						show_debug_message("on the thingy. " + string(_move.GetPosition()) + string(current_time))
					}*/
					return p
				}
				return _move.PassYLeaveCheck(-GROUNDED_THRESHOLD)
			}

			return false
		}
	}
	
	static IsGrounded = function(_passCount = true)
	{
		return velocity.y <= 0 and CheckGround(_passCount)
	}
	
	static JumpValid = function()
	{
		return IsGrounded()
	}
	
	static Jump = function()
	{
		return new Vector(0, jumpSpeed)
	}
	
	static Walk = function(_horizontal)
	{
		var _speed = _horizontal * walkSpeed //the intended speed that the player wants to go
		var _accel = acceleration * _speed //how much the player is going to accelerate by
		var _friction = sign(-velocity.x) * walkSpeed;
		
		if (IsGrounded()) _friction = _friction * FRICTION_GROUND; 
		else _friction = _friction * FRICTION_AIR 
		
		var _x = velocity.x //store the original walk speed here. Will be used for transformation.
		
		if (_speed == 0 and _x != 0) //if there's no acceleration, slow down the object to a stop
		{
			_x += _friction 
			if (sign(_x) == sign(_friction)) _x = 0 //stop the object if new velocity is in the same direction as friction 
		}
		else if (_speed != 0)
		{			
			var _diff = _speed - _x //calculate the difference ins speeds
			_x += abs(_accel) * sign(_diff) // move towards the intended speed

			if (sign(_speed - _x) != sign(_diff)) //if the difference flipped at any point
				_x = _speed //set the current speed

		}
		
		return new Vector(_x, 0)
	}
	
	static GravityAffect = function(_v)
	{
		if(_v.y >= -TERMINAL_VELOCITY)
			_v.y = max (_v.y - G, -TERMINAL_VELOCITY)
		
		return _v
		
	}
	
	
	static DashValid = function(_direction)
	{

		var is_grounded	= IsGrounded(false) 
		
		if (is_grounded)
		{
			if(_direction.Normalized().IsEqual(new Vector(0, 1))) return false //can't dash downwards if you're on the ground
			
			return true // you can dash if you're on the ground
		}
		
		return dashAir // you can dash if you have a valid air dash.

	}
	
	static DashStart = function(_direction)
	{
		var dash_speed = dashMultiplier * walkSpeed
		
		var _dir = INPUT_VECTOR
		
		if (_dir = new Vector()) _dir.x = 1 //dash to the right by default
		
		
		if (not IsGrounded() or _dir.Normalized().IsEqual(new Vector(0, -1))) dashAir = false
		_dir = _dir.Normalized()
		//dash_cd_time = dashCD
		//dash_time = dashTimer
		dash_timer.ResetTimer()
		control = 0
		gravAffect = false
		moveState = MoveState.DASH
		
		return new Vector(_dir.x * dash_speed, - _dir.y * dash_speed)
	}

	static DashEnd = function(_hardStop = false)
	{
		control = 1
		gravAffect = true
		moveState = MoveState.MOVE	
		dashAir = IsGrounded()

		if (_hardStop) return new Vector() //if we're hard stopping, then stop completely.
		
		velocity.x = sign(velocity.x) * walkSpeed

		
		if (velocity.y > 0)
		{
			velocity.y = dashMultiplier * walkSpeed / 2
		}
		else
		{
			velocity.y = min(0, velocity.y)	
		}
		
		return Move(false)
		
	}

	static Dash = function()
	{
		if(dash_timer.TimeOut() and DashPassing()) 
			return DashEnd()
		else return velocity
	}
	
	static DashPassing = function()
	{
		if (passing)
			return true
			
		with (Instance())
		{
			if (place_meeting(x, y, OPASSABLE))
				return false
		}
		return true
	}
	
	static Move = function(dashCheck = true)
	{
		var _v = velocity
		
		if(dashCheck and INPUT_DASH_PRESSED and DashValid(INPUT_VECTOR))
		{
			return DashStart(INPUT_VECTOR) //dash instead of moving if you're able to dash
		}
		
		_v.x = Walk(INPUT_HORIZONTAL).x
		
		if (IsGrounded())
		{
			if (INPUT_JUMP_PRESSED and JumpValid())
				_v.y = Jump().y
		}
		else
		{
			_v.y = GravityAffect(_v).y
		}
		
		return _v
	}
	
	static MovementUpdate = function()
	{
		var _v = velocity
		
		switch(moveState)
		{
			case MoveState.MOVE:
				_v = Move()
				break
			case MoveState.DASH:
				_v = Dash()
		}

		Movement(_v, 1)	
		
	}
	
	static MovementEndUpdate = function()
	{
		if (not dashAir)
		{
			dashAir = IsGrounded()
		}
	}
	
	static Movement = function(_v, _time_scale = 1)
	{
		var sx = _v.x * _time_scale
		var sy = _v.y * _time_scale
		
		sx = MotionHorizontalNormal(sx)
		sy = MotionVerticalNormal(sy)
		velocity.Set(sx, sy)
	}
	
	static PassXLeaveCheck = function(_x)
	{

		with (Instance())
		{
			var edge = _x < 0 ? bbox_left : bbox_right
			return not collision_line(edge + _x, bbox_top, edge + _x, bbox_bottom, OPASSABLE, false, true)
		}
	}
	
	static MotionHorizontalPassing = function(_x, _maxIterations = 9999)
	{
		var _move = self
		with (Instance())
		{
			if(_move.PassXLeaveCheck(_x))
			{
				var edge = _x < 0 ? bbox_left : bbox_right
				if (_move.moveState == MoveState.DASH)
				{
					_move.passing = false
					return true
				}
				var _a = 0
				
				while (collision_line(edge + _a, bbox_top, edge + _a, bbox_bottom, OPASSABLE, false, true) and abs(_a) <= _maxIterations)
				{
					_a += sign(_x)
				}
				x += _a - sign(_x)
				return false
			}
		}
		return true
	}
	
	static MotionHorizontalNormal = function(_x, _timeScale = 1, _maxIterations = 9999)
	{
		if (_x == 0) return 0
		
		var _move = self //a reference variable for use in with statements
		var inst = Instance()
		
		var sx = _x * _timeScale
		var _iter = 0
		
		with (inst)
		{
			if (place_meeting(x + sx, y, OSOLID))
			{	

				while(not place_meeting(x + sign(sx), y, OSOLID) and _iter <= abs(sx) * 2)
				{
					x += sign(sx) 
				}	
				sx = 0
				_x = 0
				_iter = 0
			}
	
			if (place_meeting(x + sx, y, OPASSABLE))
			{		
				if (_move.passing)
				{
					if(not _move.MotionHorizontalPassing(_x)) //if the object is being stopped by the container
					{
						sx = 0
						_x = 0
					}
				}
				else if (_move.moveState == MoveState.DASH)
				{
					_move.passing = true
				}
				else
				{
					while(not place_meeting(x + sign(sx), y, OPASSABLE) and _iter <= abs(sx) * 2)
					{
						x += sign(sx) 
					}	
					sx = 0
					_x = 0
					_iter = 0
				}
			}
			else
			{
				_move.passing = false	
			}
		}
		inst.x += round(_x)
		return _x
	}
	
	static PassYLeaveCheck = function(_y)
	{

		with (Instance())
		{
			var edge = _y < 0 ? bbox_bottom : bbox_top
			return not collision_line(bbox_left, edge - _y, bbox_right, edge - _y, OPASSABLE, false, true)
		}
	}
	static MotionVerticalPassing = function(_y, _maxIterations = 9999)
	{
		var _move = self
		
		with (Instance())
		{
			var edge = _y < 0 ? bbox_bottom : bbox_top
			
			if(not collision_line(bbox_left, edge - _y, bbox_right, edge - _y, OPASSABLE, false, true))
			{
				if (_move.moveState == MoveState.DASH)
				{
					_move.passing = false
					return true
				}
				var _a = 0
				
				while (collision_line(bbox_left, edge - _a, bbox_right, edge - _a, OPASSABLE, false, true) and abs(_a) < _maxIterations)
				{
					_a += sign(_y)
				}
				y -= _a - sign(_y)
				return false
			}
		}
		return true
	}
	
	static MotionVerticalNormal = function(_y, _timeScale = 1, _maxIterations = 9999)
	{
		if (_y == 0) return 0
		var _move = self
		
		var sy = _y * _timeScale
		var _iter = 0
		var inst = Instance()
		
		with (inst)
		{
			if (place_meeting(x, y - _y, OSOLID))
			{	
				
				while(not place_meeting(x, y - sign(_y), OSOLID) and _iter <= abs(sy) * 2)
				{
					y -= sign(_y)		
				}	
				sy = 0
				_y = 0
				_iter = 0
			}
			if (place_meeting(x, y - sy, OPASSABLE))
			{		
				if (_move.passing)
				{
					if(not _move.MotionVerticalPassing(_y, _maxIterations)) //if the object is being stopped by the container
					{
						sy = 0
						_y = 0
					}
				}
				else if (_move.moveState == MoveState.DASH)
				{
					_move.passing = true
				}
				else
				{
					while(not place_meeting(x, y - sign(sy), OPASSABLE) and _iter <= abs(sy) * 2)
					{
						y -= sign(sy)		
					}	
					sy = 0
					_y = 0
					_iter = 0
				}
			}
			else
			{
				_move.passing = false	
			}
		}	

		inst.y -= round(_y)
		return _y
	}
}

