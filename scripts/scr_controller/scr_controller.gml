/*
	NAME:			SCR_INPUT_DEFAULTS.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		2.0.0
	LAST MODIFIED:	4 SEPTEMBER 2020
*/

///BUTTON EDIT: the behavior of the controller when editing a configuration
enum ButtonEdit {
	NONE,
	SWAP,
	ERROR
}

//macros for pausing and restarting. Not necessary.
#macro PAUSE "pause"
#macro RESTART "restart"


//	CONTROLLER(_CONFIG, _SLOT)
	// Creates a controller based on the config given to it.
	// is the main thing used when getting inputs.
function Controller(_config, _slot) constructor {
	
	usePad = false
	
	// SET CONFIG: Sets the buttons based on the provided configuration
		//	_C: THE GIVEN CONFIG
		//	_S: THE CONTROLLER SLOT
	static SetConfig = function(_c, _s) {

		for(var i = 0; i < Action._length; ++i)
		{
			var _name = ActionString(i)
			
			if(variable_struct_exists(_c, _name))
			{
				
				var _a = variable_struct_get(_c, _name)
				var _n = new ButtonAction(_name, _a.key, _a.isMouse, _a.pad, _s)

				variable_struct_set(self, _name, _n)	
			}
		}
		config = _c
		slot = _s
	}
	
	// GET CONTROL: Provides the button that's associated with the action
		//	_ACTION: THE REQUESTED ACTION
		//	RETURN: THE BUTTON ASSOCIATED WITH THAT ACTION
	static GetControl = function(_action) {
		var _name = ActionString(_action)
		if (_name == undefined) 
		{
			if (variable_struct_exists(self, _action))
			{
				return variable_struct_get(self, _action)
			}	
			return noone
		}
		return variable_struct_get(self, _name)
	}

	// GET HORIZONTAL: returns the horizontal movement based on both axis and buttons
		//prioritized axis control over normal control
		
		//RETURNS:
			// -1 - 0 if moving left
			// 0 - 1 if moving right
	static GetHorizontal = function() {
		
		var _h = axisLH.GetAxisValue(false)
		
		if (_h != 0 and _h != undefined)
		{
			usePad = true
			return _h
		}
		if(variable_struct_exists(self, "right") and variable_struct_exists(self, "left"))
		{
			var _x = right.Check() - left.Check()
			if(left.Check())
			{
				usePad = left.joyPressed
			}
			else if (right.Check())
			{
				usePad = right.joyPressed
			}
			return _x
		}
		return 0
	}
	
	// GET VERTICAL: returns the vertical movement based on both axis and buttons
		//prioritized axis control over normal control
		
		//RETURNS:
			// -1 - 0 if moving up
			// 0 - 1 if moving down
	static GetVertical = function()	{
		var _v = axisLV.GetAxisValue(false)
		
		if (_v != 0 and _v != undefined)
		{
			usePad = true
			return _v
		}
		
		var _y = down.Check() - up.Check()
		if(up.Check())
		{
			usePad = up.joyPressed
		}
		else if (down.Check())
		{
			usePad = down.joyPressed
		}
		return _y
	}
	
	// CHECK: checks if designated button in question is being pressed.
		//RETURNS true if the button is currently held down
	static Check = function(_action) {
		
		if(GetControl(_action).Check())
		{
			usePad = GetControl(_action).joyPressed
			return true
		}
		return false
	}
	
	// CHECK: checks if designated button in question has been pressed.
		//RETURNS true if the button was just pressed
	static CheckPressed = function (_action) {		
		if(GetControl(_action).CheckPressed())
		{
			usePad = GetControl(_action).joyPressed
			return true
		}
		return false
	}

	static GetAxis = function(_axis) {
		switch(_axis)
		{
			case Axis.LX:
				return axisLH.GetAxisValue(false)
			case Axis.LY:
				return axisLV.GetAxisValue(false)
			case Axis.RX:
				return axisRH.GetAxisValue(false)
			case Axis.RY:
				return axisRV.GetAxisValue(false)
		}
		return undefined
	}

	#region Config Buttons
	SetConfig(_config, _slot)
	
	// CONFIG BUTTONS: buttongs and axies that won't change based on config
		// ex: axies and pausing
	
	axisLH	=	new JoystickAxis("LX", gp_axislh, slot)
	axisLV	=	new JoystickAxis("LY", gp_axislv, slot)
	axisRH	=	new JoystickAxis("RX", gp_axisrh, slot)
	axisRV	=	new JoystickAxis("RY", gp_axisrv, slot)

	pause	=	new ButtonAction(PAUSE, vk_escape, false, gp_start, slot)
	restart	=	new ButtonAction(RESTART, vk_backspace, false, gp_select, slot)
	#endregion config buttons
	
}

#region Button Structs

///BUTTONACTION()
	// create actions for the controller 
	// used for button checking.
function ButtonAction(_name, _key, _mb, _joy, _slot) constructor {
	name = _name
	slot = 0
	joystick = new JoyPadButton(_name, _joy, slot)
	key = new KeyboardButton(_name, _key, _mb)
	
	joyPressed = false //whether the last press was a joystick or a keyboard
	
	// CHECK: checks if button is being pressed.
		//RETURNS true if the button is currently held down
	static Check = function() {
		if (key.Check())
		{
			joyPressed = false
			return true	
		}
		if (joystick.Check())
		{
			joyPressed = true
			return true
		}
		return false
	}
	
	// CHECK: checks if button has been pressed.
		//RETURNS true if the button was just pressed
	static CheckPressed = function() {
		if (key.CheckPressed())
		{
			joyPressed = false
			return true	
		}
		if (joystick.CheckPressed())
		{
			joyPressed = true
			return true
		}
		return false
	}
}

#endregion

