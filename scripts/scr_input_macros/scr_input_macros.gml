/*
	NAME:			SCR_INPUT_MACROS.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		2.0.1 (Added Left Stick Macro)
	LAST MODIFIED:	24 OCTOBER 2020
*/

/// Please feel free to use space under input macros for any other macro/input use
	
// ===================== PLACE JOYSTICK MACROS HERE ==================== //
#macro JOYSTICK_DEADZONE 0.1 //use this for joystick deadzone
// ============================ END  MACROS ============================ //
	
// ====================== PLACE INPUT MACROS HERE ====================== //
#macro INPUT_UP_PRESSED global.Controls.CheckPressed(Action.UP)
#macro INPUT_UP global.Controls.Check(Action.UP)

#macro INPUT_DOWN_PRESSED global.Controls.CheckPressed(Action.DOWN)
#macro INPUT_DOWN global.Controls.Check(Action.DOWN)

#macro INPUT_LEFT_PRESSED global.Controls.CheckPressed(Action.LEFT)
#macro INPUT_LEFT global.Controls.Check(Action.LEFT)

#macro INPUT_RIGHT_PRESSED global.Controls.CheckPressed(Action.RIGHT)
#macro INPUT_RIGHT global.Controls.Check(Action.RIGHT)

#macro INPUT_JUMP_PRESSED global.Controls.CheckPressed(Action.JUMP)
#macro INPUT_JUMP global.Controls.Check(Action.JUMP)

#macro INPUT_DASH_PRESSED global.Controls.CheckPressed(Action.DASH)
#macro INPUT_DASH global.Controls.Check(Action.DASH)

#macro INPUT_PREVIOUS_PRESSED global.Controls.CheckPressed(Action.PREVIOUS)
#macro INPUT_PREVIOUS global.Controls.Check(Action.PREVIOUS)

#macro INPUT_NEXT_PRESSED global.Controls.CheckPressed(Action.NEXT)
#macro INPUT_NEXT global.Controls.Check(Action.NEXT)

#macro INPUT_HORIZONTAL global.Controls.GetHorizontal()
#macro INPUT_VERTICAL global.Controls.GetVertical()

#macro INPUT_VECTOR new Vector(INPUT_HORIZONTAL, INPUT_VERTICAL)

#macro INPUT_PAUSE_PRESSED global.Controls.CheckPressed(PAUSE)
#macro INPUT_PAUSE global.Controls.Check(PAUSE)

#macro INPUT_RESTART_PRESSED global.Controls.CheckPressed(RESTART)
#macro INPUT_RESTART global.Controls.Check(RESTART)


#macro INPUT_LEFT_STICK new Vector(global.Controls.GetAxis(Axis.RX), global.Controls.GetAxis(Axis.RY))
#macro INPUT_RIGHT_STICK new Vector(global.Controls.GetAxis(Axis.RX), global.Controls.GetAxis(Axis.RY))

#macro INPUT_JOYPAD_CHECK global.Controls.usePad
