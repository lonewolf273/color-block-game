// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

#macro COLORSCHEME_DEFAULT new ColorScheme()
#macro COLORSCHEME_REVERSE new ColorScheme(c_white, c_gray, c_black, c_red, c_green)
#macro COLORSCHEME_ALT2 new ColorScheme($324129, $5a803d, $e3dbbe, $6c4dee, c_gray)
#macro COLORSCHEME_ALT1 new ColorScheme(ToBGR($274c77), ToBGR($a3cef1), ToBGR($e7ecef), ToBGR($6096ba), ToBGR($8b8c89))

function ColorScheme(_background = c_black, _solid = c_gray, _passable = c_white, _flag = c_green, _enemy = c_red) constructor {
	background = _background
	solid = _solid
	passable = _passable
	flag = _flag
	enemy = _enemy
}

function ColorSchemeList() constructor
{
	list = 
	[
		COLORSCHEME_DEFAULT,
		COLORSCHEME_REVERSE,
		COLORSCHEME_ALT1,
		COLORSCHEME_ALT2
	]
	
	index = 0
	
	static GetScheme = function()
	{
		if (array_length(list) < 0) return noone
		return list[index]	
	}
	
	static PreviousScheme = function()
	{
		
		var l = array_length(list)
		if (l == 0)
			return noone
		
		index = index - 1
		
		if (index < 0)
			index = l - 1
		
		return GetScheme()
	}
	
	static NextScheme = function()
	{
		
		var l = array_length(list)
		if (l == 0)
			return noone
		
		index = index + 1
		
		if (index >= l)
			index = 0
		
		return GetScheme()
	}
}

function ToBGR(val)
{
	return (val & $FF) << 16 | (val & $FF00) | (val & $FF0000) >> 16;
}
