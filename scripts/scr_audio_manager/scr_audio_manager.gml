// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function MusicGroup() constructor
{
	syncGroup = noone
	soundList = []
	nowPlaying = -1
	paused = false
	
	Clear()
	
	static NowPlaying = function()
	{
		return soundList[nowPlaying]	
	}
	
	static CurrentGain = function()
	{
		audio_sound_get_gain(NowPlaying())	
	}
	
	static Play = function(soundid, gain = 1)
	{
		if (syncGroup == noone)
			syncGroup = audio_create_sync_group(true)	
		
		var found = false
		for (var i = 0, len = array_length(soundList); i < len; ++i)
		{
			if(soundList[i] == soundid)
			{
				audio_sound_gain(soundList[i], gain, 0)
				found = true
				nowPlaying = i
			}
			else
				audio_sound_gain(soundList[i], 0, 0)
		}
		
		if (not found)
		{
			audio_play_in_sync_group(syncGroup, soundid)
			audio_sound_gain(soundid, gain, 0)
			var len = array_length(soundList)
			array_resize(soundList, len + 1)
			nowPlaying = len
		}

		audio_start_sync_group(syncGroup)
		
	}
	
	static Pause = function()
	{
		paused = true
		audio_pause_sync_group(syncGroup)
	}
	
	static Resume = function()
	{
		if (paused)
		{
			paused = false
			audio_resume_sync_group(syncGroup)
		}
		else
		{
			audio_start_sync_group(syncGroup)	
		}
	}
	
	static Stop = function()
	{
		if audio_sync_group_is_playing(syncGroup)
			audio_stop_sync_group(syncGroup);
	}
	
	static Clear = function()
	{
		
		if (syncGroup != noone)
		{
			Stop()
			audio_destroy_sync_group(syncGroup)
		}
		syncGroup = noone
		soundList = []
		paused = false
	}
	
	static Find = function(soundid)
	{
		for (var i = 0, len = array_length(soundList); i < len; ++i)
		{
			if(soundList[i] == soundid)
				return i
		}
		return -1
	}
	
	static ChangeGain = function(gain = 1, _time = 0)
	{
		audio_sound_gain(NowPlaying(), gain, _time)
	}
}

function Sound(_id = noone, _snd = noone) constructor
{
	id = _id	
	snd = _snd
	
}

function SoundGroup() constructor
{
	soundList = []
	
}

function AudioManager() constructor
{
	music = new MusicGroup()
	sfx = new SoundGroup()
	
	musGain = 0.5
	sfxGain = 0.5
	
	static PlayMusic = function(soundid)
	{
		music.Play(soundid, musGain)
	}
	
	static PauseMusic = function()
	{
		music.Pause()	
	}
	
	static ChangeMusicSound = function(_gain = 1)
	{
		music.ChangeGain(_gain)
	}
	
	static Clear = function()
	{
		music.Clear()	
	}
}