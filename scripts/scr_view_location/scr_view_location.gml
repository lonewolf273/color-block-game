function scr_view_location(argument0, argument1) {
	var obj = argument0
	var d = argument1

	var x1 = obj.x
	var y1 = obj.y
	var h = obj.view_height
	var w = obj.view_width

	switch(d)
	{
		case directional.north:
			return y1 - h/2
		case directional.south:
			return y1 + h/2
		case directional.west:
			return x1 - w/2
		default:
			return x1 + w/2
	}


}
