/*
	NAME:			SCR_KEYPAD_CONTROL.GML
	AUTHOR:			SKY TRICKSTER
	VERSION:		2.0.0
	LAST MODIFIED:	9 NOVEMBER 2021
*/


#region KeyboardButtons

/// KEYBOARDBUTTON(NAME, KEY, ISMOUSE)
	//used to check the action's keyboard presses
function KeyboardButton(_name, _key, _isMouse) constructor {
	name = _name
	is_number = false
	key = ValidKey(_key)
	mouseCheck = _isMouse
	
	
	static ValidKey = function(k)
	{
		if (is_string(k))
		{
			if (not ValidNumber(k)) return ord(k)
			
			is_number = true
		}
		return k
	}
	
	static CheckPressed = function()
	{
		if (is_number) return keyboard_check_pressed(ord(key))
		return mouseCheck ? mouse_check_button_pressed(key) : keyboard_check_pressed(key)
	}
	
	static Check = function()
	{
		if (is_number) return keyboard_check(ord(key))
		return mouseCheck ? mouse_check_button(key) : (is_number ? keyboard_check(ord(key)) : keyboard_check(key))
	}
	
	static ValidNumber = function(k)
	{
		if (ord(k) >= ord("0") and ord(k) <= ord("9")) return true
		return false
	}
}
#endregion
