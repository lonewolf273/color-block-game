// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Vector(_x = 0, _y = 0) constructor
{
	x = _x
	y = _y
	
	static Set = function(_x, _y)
	{
		x = _x
		y = _y
	}
	
	static Length = function(v)
	{
		return point_distance(0, 0, self.x, self.y)	
	}
	
	static DistanceTo = function(v)
	{
		return point_distance(self.x, self.y, v.x, v.y)	
	}
	
	static Add = function(v)
	{
		return new Vector(self.x + v.x, self.y + v.y)	
	}
	
	static Normalized = function()
	{
		var l = self.Length()
		if (l == 0) return new Vector(0, 0)
		return new Vector(self.x / l, self.y / l)
	}
	
	static IsEqual = function(v)
	{
		return v.x == self.x and v.y == self.y
	}
	
	static toString = function()
	{
		return "[" + string(self.x) + ", " + string(self.y) + "]"	
	}
}