function scr_player_walk() {

	//amount of movement is the speed at which you are moving * the control
	//arg[0] is left
	//arg[1] is right

	var x_intend_raw = (argument[1] - argument[0]) * control

	var a = acceleration * x_intend_raw * walk_speed //current acceleration
	var f = grounded ? friction_ground * sign(-xspeed) * walk_speed : friction_air * sign(-xspeed) * walk_speed
	var z = a == 0


	// store the original speed
	var v = xspeed

	// if moving and not accelerating, slow down to a stop
	if z and xspeed != 0 
	{
		v = xspeed + f

		if (sign(v) != sign(xspeed)) v = 0
	}
	else if (not z) //
	{	
		// check same direction.
		// true if same direction
		var d = sign(xspeed) == sign(a)

		if (d and abs(xspeed) > walk_speed) //
		{
			v = xspeed - sign(xspeed) * f //slow down 
		
			if (v < walk_speed)
				v = walk_speed * sign(xspeed)
		}
		else if (not (d and abs(v) == walk_speed))
		{
			v = xspeed + a

			if (d and abs(v) > walk_speed)
				v = walk_speed * sign(v)
		}
	}

	return v


}
